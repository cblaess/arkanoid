#ifndef _VIE_H_
#define _VIE_H_

#include <stdbool.h>

typedef struct Vie Vie;

struct Vie
{
	int nbVie;
	bool vivant;
};

void AugmenterVie (Vie* v);
void BaisserVie (Vie* v);

#endif
