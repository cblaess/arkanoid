#ifndef _BALLE_H_
#define _BALLE_H_

#include "vie.h"
#include <SDL.h>

typedef struct Balle Balle;

struct Balle
{
	double x;
	double y;
	double vx;
	double vy;
	double diametre;
	SDL_Rect srcBall;
};

void CollisionBordB (Balle* b, Vie* v, SDL_Surface* win_surf);
void DeplacementDebutPartie(Balle* b, double x);
void Deplacement (Balle* b, double delta_t);

#endif
