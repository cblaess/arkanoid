#ifndef _VAISSEAU_H_
#define _VAISSEAU_H_

#include <SDL.h>
#include "balle.h"
#include <stdbool.h>

typedef struct Vaisseau Vaisseau;

struct Vaisseau
{
	double x;
	double y;
	SDL_Rect srcVaiss;
};

void CollisionBordV (Vaisseau* v, SDL_Rect* dest, SDL_Surface* win_surf);
void CollisionBalle (Vaisseau* v, Balle* b, SDL_Rect* dest, bool firstShoot);


#endif