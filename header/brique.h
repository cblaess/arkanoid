#ifndef _BRIQUE_H_
#define _BRIQUE_H_

typedef struct Brique Brique;

struct Brique
{
  //Etat de la brique (1 = active, 0 = pas de brique)
  int etat;
  //Type de brique 
  int type;
  //Solidite de la brique (nb de fois qu'il fait la toucher pour qu'elle se casse)
  int solidite;
  //Nb points de la brique
  int points;
};

#endif
