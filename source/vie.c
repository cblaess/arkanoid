#include <stdio.h>
#include <stdlib.h>
#include "vie.h"

void AugmenterVie (Vie* v)
{
	v->nbVie++;
}

void BaisserVie (Vie* v)
{
	v->nbVie--;

	if(v->nbVie == 0)
	{
		v->vivant = false;
	}
}