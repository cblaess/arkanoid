#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "balle.h"
#include "vie.h"

void CollisionBordB (Balle* b, Vie* v, SDL_Surface* win_surf)
{
	if ((b->x < 1) || (b->x > (win_surf->w - b->diametre)))
	{
		b->vx *= -1;
	}
	if ((b->y < 1))
	{
		b->vy *= -1;
		
	}
	if (b->y > (win_surf->h - b->diametre))
	{
		b->vy *= -1;

		BaisserVie(v);
	}
}

void DeplacementDebutPartie(Balle* b, double x)
{
	 b->x = x;
}

void Deplacement(Balle* b, double delta_t)
{
	b->x += b->vx / delta_t;
	b->y += b->vy / delta_t;
}
