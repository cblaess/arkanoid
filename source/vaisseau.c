#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include "vaisseau.h"

void CollisionBordV (Vaisseau* v, SDL_Rect* dest, SDL_Surface* win_surf)
{
	if (dest->x < 1)
	{
		dest->x = 1;
	}
	if (dest->x > (win_surf->w - v->srcVaiss.w))
	{
		dest->x = win_surf->w - v->srcVaiss.w;
	}
}

void CollisionBalle (Vaisseau* v, Balle* b, SDL_Rect* dest, bool firstShoot)
{
	if(!firstShoot)
	{
		if(b->y + b->diametre > dest->y && (b->x >=  dest->x && b->x <= dest->x + v->srcVaiss.w))
		{
			b->vy *= -1;

			if(b->vx == 0 && b->x < dest->x + (v->srcVaiss.w / 2))
			{
				b->vx = 1;
			}
			else if(b->vx == 0 && b->x > dest->x + (v->srcVaiss.w / 2))
			{
				b->vx = -1;
			}

			if(b->x < dest->x + (v->srcVaiss.w / 2) && b->vx > 0)
			{
				b->vx *= -1;
			}
			else if(b->x > dest->x + (v->srcVaiss.w / 2) && b->vx < 0)
			{
				b->vx *= -1;
			}
		}
	}
}
