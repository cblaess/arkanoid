#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include "balle.h"
#include "brique.h"
#include <math.h>
#include "vie.h"
#include "vaisseau.h"

#define HAUTEUR_MAP 20
#define LARGEUR_MAP 13
#define HAUTEUR_BRIQUE 16
#define LARGEUR_BRIQUE 32

//signbit(val)

//struct { double x; double y; } ball_speed;
int niveau; // Niveau que le joueur joue
int niveauMax;    // Niveau max avant la fin de la partie
bool victoire;    // Booléen qui indique si le joueur a gagné

Vaisseau vaiss;
Balle ball;
Vie vie;

int nbBrique;    // Nombre de brique sur la carte

Uint64 prev, now; // timers
double delta_t;  // durée frame en ms
bool firstShoot;

SDL_Window* pWindow = NULL;
SDL_Surface* win_surf = NULL;
SDL_Surface* plancheSprites = NULL;
SDL_Surface* plancheGraphiques = NULL;
SDL_Surface* plancheASCII = NULL;

SDL_Rect srcBg = { 0,128, 96,128 }; // x,y, w,h (0,0) en haut a gauche

int y = 0;
Brique map[20][13];
int scoreJoueur = 0;

void init()
{
    pWindow = SDL_CreateWindow("Arkanoid", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 600, 600, SDL_WINDOW_SHOWN);
    win_surf = SDL_GetWindowSurface(pWindow);
    plancheSprites = SDL_LoadBMP("../ressource/sprites.bmp");
    plancheGraphiques = SDL_LoadBMP("../ressource/Arkanoid_sprites.bmp");
    plancheASCII = SDL_LoadBMP("../ressource/Arkanoid_ascii.bmp");
    SDL_SetColorKey(plancheSprites, true, 0);  // 0: 00/00/00 noir -> transparent
    SDL_SetColorKey(plancheGraphiques, true, 0);
    SDL_SetColorKey(plancheASCII, true, 0);
    
    niveau = 2;    // Indique la partie commence avec le premier niveau
    niveauMax = 2;    // Indique que le niveau max est le deuxième niveau
    nbBrique = 0;    // Initialise le nombre de brique
    victoire = false;    // Initialise la victoire à false
    
    vaiss.srcVaiss.x = 128;
    vaiss.srcVaiss.y = 0;
    vaiss.srcVaiss.w = 128;
    vaiss.srcVaiss.h = 32;
    vaiss.x = (win_surf->w / 2) - (vaiss.srcVaiss.w / 2);
    vaiss.y = win_surf->h - vaiss.srcVaiss.h;
    
    ball.diametre = 25;
    ball.srcBall.x = 0;
    ball.srcBall.y = 96;
    ball.srcBall.w = 24;
    ball.srcBall.h = 24;
    ball.x = vaiss.x + (vaiss.srcVaiss.w / 2) - (ball.diametre / 2);
    ball.y = vaiss.y - ball.diametre;
    ball.vx = 0;
    ball.vy = 0;
    
    vie.nbVie = 3;
    vie.vivant = true;
    
    firstShoot = true;
}

// Fonction qui remet la balle au centre du vaisseau et qui permet d'être tirée en cliquant sur la souris
void InitBalle()
{
    firstShoot = true;
    ball.x = vaiss.x + (vaiss.srcVaiss.w / 2) - (ball.diametre / 2);
    ball.y = vaiss.y - ball.diametre;
    ball.vx = 0;
}

void loadLevel()
{
    char chaineNiveau[50]; // Chaine qui correspont au niveau à charger
    
    sprintf(chaineNiveau, "../ressource/level%d.txt", niveau); // Initialise la chaine avec le niveau
    
    FILE *fp = fopen(chaineNiveau, "r");
    if (fp == NULL)
    {
        printf("Failed to open map\n");
    }
    for (int x = 0; x < HAUTEUR_MAP; x++)
    {
        for (int y = 0; y < LARGEUR_MAP; y++)
        {
            fscanf(fp, "%d", &map[x][y].type);
            // printf("%d,",map[x][y].type);
            
            if(map[x][y].type == 0)
            {
                map[x][y].etat = 0;
            }
            else
            {
                nbBrique++;
                map[x][y].etat = 1;
                
                if(map[x][y].type <= 12)
                {
                    map[x][y].solidite = 1;
                }
                else if(map[x][y].type <= 18)
                {
                    map[x][y].solidite = 2;
                }
                else if(map[x][y].type <= 24)
                {
                    map[x][y].solidite = 3;
                }
            }
        }
    }
    
    niveau++; // Incrémente le niveau si le joueur termine le niveau courant
    
    fclose(fp);
}

void loadMap()
{
    for(int x = 0; x < HAUTEUR_MAP; x++) // 0 .. 19
    {
        for(int y = 0; y < LARGEUR_MAP; y++) // 0 .. 12
        {
            if(map[x][y].etat == 1)
            {
                int etatBrique = map[x][y].type;
                int valY = 0;
                if(etatBrique <= 6)
                {
                    valY = 0;
                }
                else if(etatBrique <= 12)
                {
                    valY = 1;
                }
                else if(etatBrique <= 18)
                {
                    valY = 2;
                }
                else if(etatBrique <= 24)
                {
                    valY = 3;
                }
                
                SDL_Rect depart = { ((etatBrique - 1) % 6) * LARGEUR_BRIQUE, valY * HAUTEUR_BRIQUE, LARGEUR_BRIQUE, HAUTEUR_BRIQUE };
                SDL_Rect destination = { y * LARGEUR_BRIQUE, x * HAUTEUR_BRIQUE, LARGEUR_BRIQUE, HAUTEUR_BRIQUE};
                SDL_BlitSurface(plancheGraphiques, &depart, win_surf, &destination);
            }
        }
    }
}


void majScore()
{
    char score[5];
    sprintf(score, "%d", scoreJoueur);
    
    for(int i = 4; i >= 0; i--)
    {
        if(score[i] != '\0')
        {
            int val = (score[i] - '0');
            SDL_Rect depart = { 2 + val * 32, 38, 14, 20};
            SDL_Rect destination = { 500 + i * 20, 15, 14, 20};
            SDL_BlitSurface(plancheASCII, &depart, win_surf, &destination);
        }
    }
}

void affichageVie()
{
    for(int i = 0; i < vie.nbVie; i++)
    {
        SDL_Rect depart = { 80, 66, 16, 12};
        SDL_Rect destination = { 520 + i * 20, 50, 16, 12};
        SDL_BlitSurface(plancheGraphiques, &depart, win_surf, &destination);
    }
}

// Fonction qui gère la collision de la balle avec les briques
bool gestionCollision(Balle prevPos)
{
    bool continuer = true; // Booléen qui indique si la partie doit continuer ou pas
    
    // On n'a pas besoin de faire de test pour savoir si la balle touche une brique en dessous du tableau de brique
    // 5 : marge de securite
    if(ball.y < (HAUTEUR_MAP * HAUTEUR_MAP + 5))
    {
        /*   int xTest;
         int yTest;
         
         if(signbit(ball.vx) == 1 && signbit(ball.vy) == 1)
         {
         xTest = (int)floor(ball.y/HAUTEUR_BRIQUE);
         yTest = (int)floor(ball.x/LARGEUR_BRIQUE);
         //printf("⇖\n");
         }
         else if(signbit(ball.vx) == 1 && signbit(ball.vy) == 0)
         {
         xTest = (int)ceil(ball.y/HAUTEUR_BRIQUE);
         yTest = (int)floor(ball.x/LARGEUR_BRIQUE);
         //printf("⇙\n");
         }
         else if(signbit(ball.vx) == 0 && signbit(ball.vy) == 1)
         {
         xTest = (int)floor(ball.y/HAUTEUR_BRIQUE);
         yTest = (int)ceil(ball.x/LARGEUR_BRIQUE);
         //printf("⇗\n");
         }
         else// if(signbit(ball.vx) == 0 && signbit(ball.vy) == 0)
         {
         xTest = (int)floor(ball.y/HAUTEUR_BRIQUE);
         yTest = (int)floor(ball.x/LARGEUR_BRIQUE);
         //printf("⇘\n");
         }
         
         if(map[xTest][yTest].etat == 1 && map[xTest][yTest].solidite > 1)
         {
         map[xTest][yTest].solidite--;
         ball.vy *= -1;
         }
         else if(map[xTest][yTest].etat == 1 && map[xTest][yTest].solidite == 1)
         {
         map[xTest][yTest].etat = 0;
         nbBrique--; // Si une brique casse on décrémente le nombre de brique
         ball.vy *= -1;
         }
         if(nbBrique == 0 && niveau <= niveauMax) // Si il y a encore des niveau on charge le nouveau niveau et on initialise la balle
         {
         InitBalle();
         loadLevel();
         }
         else if(nbBrique == 0 && niveau > niveauMax) // Si il n'y a plus de niveau on initialise la balle, on met la variable victoire à true et la variable continuer à false
         {
         InitBalle();
         victoire = true;
         continuer = false;
         }
         }*/
        
        double alphaMin = 9999999;
        int briqueSupprX = -1;
        int briqueSupprY = -1;
        int coteCollision = -1;
        // 20
        for(int i = 0; i < HAUTEUR_MAP; i++)
        {
            double alphaY = (i * HAUTEUR_BRIQUE - ball.y) / ball.vy;
            double alphaY2 = ((i + 1) * HAUTEUR_BRIQUE - ball.y) / ball.vy;
            
            double qYmin = (i * HAUTEUR_BRIQUE);
            double qYmax = ((i + 1) * HAUTEUR_BRIQUE);
            
            // 13
            for(int j = 0; j < LARGEUR_MAP; j++)
            {
                // on regarde toutes les briques actives
                if(map[i][j].etat == 1)
                {
                    double alphaX = (j * LARGEUR_BRIQUE - ball.x) / ball.vx;
                    double alphaX2 = ((j + 1) * LARGEUR_BRIQUE - ball.x) / ball.vx;
                    
                    double qXmin = (j * LARGEUR_BRIQUE);
                    double qXmax = ((j + 1) * LARGEUR_BRIQUE);
                    
                    double qY1 = ball.y + (alphaX * ball.vy);
                    if(qY1 >= qYmin && qY1 <= qYmax)
                    {
                        if(alphaMin > alphaX && qY1 >= 0)
                        {
                            alphaMin = alphaX;
                            briqueSupprX = i;
                            briqueSupprY = j;
                            coteCollision = 1;
                        }
                    }
                    
                    double qY2 = ball.y + (alphaX2 * ball.vy);
                    if(qY2 >= qYmin && qY2 <= qYmax)
                    {
                        if(alphaMin > alphaX2 && qY2 >= 0)
                        {
                            alphaMin = alphaX2;
                            briqueSupprX = i;
                            briqueSupprY = j;
                            coteCollision = 1;
                        }
                    }
                    
                    double qX1 = ball.x + (alphaY * ball.vx);
                    if(qX1 >= qXmin && qX1 <= qXmax)
                    {
                        if(alphaMin > alphaY && qX1 >= 0)
                        {
                            alphaMin = alphaY;
                            briqueSupprX = i;
                            briqueSupprY = j;
                            coteCollision = 2;
                        }
                    }
                    
                    double qX2 = ball.x + (alphaY2 * ball.vx);
                    if(qX2 >= qXmin && qX2 <= qXmax)
                    {
                        if(alphaMin > alphaY2 && qX2 >= 0)
                        {
                            alphaMin = alphaY2;
                            briqueSupprX = i;
                            briqueSupprY = j;
                            coteCollision = 2;
                        }
                    }
                }
            }
        }
        if(briqueSupprX != -1 && briqueSupprY != -1 && (alphaMin * alphaMin) < (1/(ball.vx * ball.vx + ball.vy * ball.vy)))
        {
            if(map[briqueSupprX][briqueSupprY].etat == 1 && map[briqueSupprX][briqueSupprY].solidite > 1)
            {
                map[briqueSupprX][briqueSupprY].solidite--;
                
            }
            else if(map[briqueSupprX][briqueSupprY].etat == 1 && map[briqueSupprX][briqueSupprY].solidite == 1)
            {
                map[briqueSupprX][briqueSupprY].etat = 0;
                nbBrique--; // Si une brique casse on décrémente le nombre de brique
                scoreJoueur += 10 + map[briqueSupprX][briqueSupprY].type * 5;
            }
            
            if(nbBrique == 0 && niveau <= niveauMax) // Si il y a encore des niveau on charge le nouveau niveau et on initialise la balle
            {
                InitBalle();
                loadLevel();
            }
            else if(nbBrique == 0 && niveau > niveauMax) // Si il n'y a plus de niveau on initialise la balle, on met la variable victoire à true et la variable continuer à false
            {
                InitBalle();
                victoire = true;
                continuer = false;
            }
            
            if(coteCollision == 2)
            {
                ball.vy *= -1;
            }
            else if (coteCollision == 1)
            {
                ball.vx *= -1;
            }
        }
    }
    return continuer;
}

// fonction qui met à jour la surface de la fenetre "win_surf"
bool draw()
{
    bool finPartie = false; // Booléen qui indique si la partie doit se terminer
    // remplit le fond
    SDL_Rect dest = { 0,0,0,0 };
    for (int j = 0; j < win_surf->h; j+=srcBg.h)
    {
        for (int i = 0; i < win_surf->w; i += srcBg.w)
        {
            dest.x = i;
            dest.y = j;
            SDL_BlitSurface(plancheSprites, &srcBg, win_surf, &dest);
        }
    }
    
    // affiche balle
    SDL_Rect dstBall = {ball.x, ball.y, 0, 0};
    SDL_BlitSurface(plancheSprites, &ball.srcBall, win_surf, &dstBall);
    
    // sauvegarde l'ancienne position de la balle pour permettre de voir si elle va traverser un bloc
    Balle prevPos = ball;
    
    // deplacement
    if(firstShoot)
    {
        DeplacementDebutPartie(&ball, vaiss.x + (vaiss.srcVaiss.w / 2) - (ball.diametre / 2));
    }
    else
    {
        Deplacement(&ball, delta_t);
    }
    //  printf("Position X avant : %f , maintenant : %f\n", prevPos.x, ball.x);
    //  printf("Position Y avant : %f , maintenant : %f\n", prevPos.y, ball.y);
    
    int vieAvant = vie.nbVie;
    // collision bord balle
    CollisionBordB(&ball, &vie, win_surf);
    
    if(vieAvant > vie.nbVie) // Si le joueur a perdu une vie
    {
        InitBalle();
        
        if(!vie.vivant) // Si le joueur n'a plus de vie
        {
            finPartie = true;
        }
    }
    
    // touche bas -> rouge
    //if (ball.y >(win_surf->h - 25))
    //{
    //    srcBall.y = 64;
    //}
    // touche haut -> vert
    //if (ball.y < 1)
    //{
    //    srcBall.y = 96;
    //}
    
    // vaisseau
    dest.x = vaiss.x;
    dest.y = vaiss.y;
    
    // collision bord vaisseau
    CollisionBordV(&vaiss, &dest, win_surf);
    
    CollisionBalle(&vaiss, &ball, &dest, firstShoot);
    
    SDL_BlitSurface(plancheSprites, &vaiss.srcVaiss, win_surf, &dest);
    
    // gestionCollision
    if(!gestionCollision(prevPos))
    {
        finPartie = true;
    }
    
    loadMap();
    majScore();
    affichageVie();
    return finPartie;
}

int main(int argc, char** argv)
{
    if (SDL_Init(SDL_INIT_VIDEO) != 0 )
    {
        return 1;
    }
    
    init();
    loadLevel();
    
    bool quit = false;
    while (!quit)
    {
        SDL_Event event;
        while (!quit && SDL_PollEvent(&event))
        {
            switch (event.type)
            {
                case SDL_QUIT:
                    quit = true;
                    break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym)
                {
                        // touche clavier
                    case SDLK_LEFT:
                        if(vaiss.x > 1)
                        {
                            vaiss.x -= 10;
                        }
                        break;
                    case SDLK_RIGHT:
                        if (vaiss.x < (win_surf->w - vaiss.srcVaiss.w))
                        {
                            vaiss.x +=10;
                        }
                        break;
                    case SDLK_ESCAPE:
                        quit = true;
                        break;
                    default:
                        break;
                }
                    break;
                case SDL_MOUSEMOTION:
                    if(event.motion.x > (vaiss.srcVaiss.w / 2) && event.motion.x < (win_surf->w - (vaiss.srcVaiss.w / 2)))
                    {
                        vaiss.x = event.motion.x - (vaiss.srcVaiss.w / 2);
                    }
                    break;
                case SDL_MOUSEBUTTONDOWN:
                    if(firstShoot)
                    {
                        firstShoot = false;
                        ball.vy = -1;
                    }
                    //printf("mouse click %d\n", event.button.button);
                    break;
                default:
                    break;
            }
        }
        prev = now;
        now = SDL_GetPerformanceCounter();
        delta_t = (double)((now - prev) * 250 / (double)SDL_GetPerformanceFrequency());
        if(draw() && !victoire)
        {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,
                                     "Partie finie !",
                                     "Vous n'avez plus de vie, vous avez perdu !",
                                     NULL);
            quit = true;
        }
        else if(victoire)
        {
            SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION,
                                     "Partie finie !",
                                     "Vous avez cassé toutes les briques ! Félicitation, vous avez remporté la partie !",
                                     NULL);
            quit = true;
        }
        
        SDL_UpdateWindowSurface(pWindow);
    }
    SDL_Quit();
    return 0;
}
